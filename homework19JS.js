const speedMembersTeam = [1, 1, 2, 1];
    const allListTasks = [5, 5, 6, 6];
    let dateFulfillTxt = '10-29-2023';

    const dateFulfill = new Date(dateFulfillTxt);
    let complete = false;
    let result = '';

    function getResult(arrSpeedMembersTeam, arrAllListTasks, date) {

      let totalTeamPoint = totalPoint(arrSpeedMembersTeam);
      let totalTasksPoint = totalPoint(arrAllListTasks);

      let speedMembersTeam_hour = totalTeamPoint / 5;
      let fullDayToFulfill = dateFulfill.getDate() - new Date().getDate();

    
      let selectDay = new Date();
      let workDayToFulfill = fullDayToFulfill;
      for (let i = 1; i <= fullDayToFulfill; i++) {
        if (selectDay.getDay() === 0 || selectDay.getDay() === 5) {
          workDayToFulfill--;
        }
        selectDay.setDate(selectDay.getDate() + 1);
      }

    
      let endTime = Math.round(totalTasksPoint / speedMembersTeam_hour) - workDayToFulfill * 5;
      if (endTime > 0) {
        complete = false;
        result = `Команде разработчиков придется потратить дополнительно ${parseInt(endTime / 8)} дня(ей) и ${Math.round(endTime) % 8} часов после дедлайна, чтобы выполнить все задачи в беклоге`;
      }
      else {
        complete = true;
        result = `Все задачи будут успешно выполнены за ${Math.abs(parseInt(endTime / 8))} дня(ей) и ${Math.abs(Math.round(endTime) % 8)} часов до наступления дедлайна!`;
      }

      return result;
    }

    function totalPoint(arr) {
      return arr.reduce((val, item) => (val + item), 0);
    }


    alert(getResult(speedMembersTeam, allListTasks, dateFulfill))
